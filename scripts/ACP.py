from sklearn import decomposition
from sklearn.decomposition import PCA
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt


from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import ExtraTreesClassifier




def accuracyVSnb_components(X, Y):
	"""
		Plot accuracy according to the number of principal components chosen for the ACP
	"""
	# Numbers of components to test
	list_nb_components = list(range(2, 10)) # from 2 to 10, step by 1

	list_accuracy_training = []	
	list_accuracy_testing = []	
	
	for n in list_nb_components:
	
		# Perform the PCA
		pca = PCA(n_components=n)
		X_reduced = pca.fit_transform(X)

		# Split data into train and test datas
		X_train, X_test, y_train, y_test = train_test_split(X_reduced, Y, test_size=0.33, stratify = Y, random_state=42)

		# Create model
		model = ExtraTreesClassifier(n_estimators=100, random_state=0)

		# Fit the model
		model.fit(X_train, y_train)

		# Test model
		Z = model.predict(X_test)
			
		# Calculate training accuracy
		training_accuracy = accuracy_score(y_train, model.predict(X_train))
		list_accuracy_training.append(training_accuracy)
		# Calculate testing accuracy
		testing_accuracy = accuracy_score(y_test, Z)
		list_accuracy_testing.append(testing_accuracy)
				
	# Plot accuracies according to the number of ACP components
	plt.style.use('ggplot')
	plt.plot(list_nb_components, list_accuracy_training, color='red', label = 'Entraînement')
	plt.plot(list_nb_components, list_accuracy_testing, color='blue', linestyle='dashed', label = 'Test')
	plt.legend()
	plt.xlabel("Nombre de composants principaux de l'ACP")
	plt.ylabel("Justesse")
	plt.savefig('../output/ACP/accuracyVSnb_components.png')
	plt.show()	
	
	
	
	
	
	


def screePlot(X):
	"""
		Plor the explained variance by a principal component, for different ACP where the number of principal components chosen is different
	"""
	n_components = [2, 3, 4, 5, 6, 10]
	
	for n in n_components:
	
		# Perform the PCA
		pca = PCA(n_components=n)
		reduced = pca.fit_transform(X)

		# Append the principle components for each entry to the dataframe
		for i in range(0, n):
		    X['PC' + str(i + 1)] = reduced[:, i]


		# Perform a scree plot
		ind = np.arange(0, n)
		plt.plot(ind, pca.explained_variance_ratio_, label="{} composants principaux".format(n), marker='o')
		
	plt.xticks(ind)
	plt.xlabel('Numéro du composant principal')
	plt.ylabel('Variance expliquée')	
	plt.legend()
	plt.style.use('ggplot')
	plt.savefig("../output/ACP/screeplots.png", dpi=199)
	plt.show()
	
	
	
	
	
	
	
	
def ACP(df, n_components):
	'''
		Reduction of dimensions by PCA
		
		@param X: expression of al genes
		@param n_composents: number of dimensions of the ACP to perform
		 
		Return 2 plots:
		1. The points in terms of the first two PCs
		2. Variable factor map for the first two dimensions.
	'''

	
	# Perform the PCA
	pca = PCA(n_components=n_components)
	X = df.iloc[:,:-1] # remove labels
	reduced = pca.fit_transform(X)

	# Append the principle components for each entry to the dataframe
	for i in range(0, n_components):
	    df['PC' + str(i + 1)] = reduced[:, i]



	
	
	
	# Show the points in terms of the first two PCs
	g = sns.lmplot(x='PC1',
		       y='PC2',
		       hue='Class',data=df,
		       fit_reg=False,
		       scatter=True,
		       height=7)


	plt.savefig('../output/ACP/firstTwoPCs_{}components.png'.format(n_components), dpi=199)
	plt.show()
	
	
	
	important_genes = ['gene_14114', 'gene_13818', 'gene_11903', 'gene_15895']
	
	#Display only the 10 first better genes
	#important_genes = ['gene_14114', 'gene_19296', 'gene_7792', 'gene_2774', 'gene_2639', 'gene_13818', 'gene_15895', 'gene_15633', 'gene_9184', 'gene_11903']

	# Display only the 50 first better genes
	#important_genes = ['gene_14114', 'gene_19296', 'gene_7792', 'gene_2774', 'gene_2639', 'gene_13818', 'gene_15895', 'gene_15633', 'gene_9184', 'gene_11903', 'gene_450', 'gene_8891', 'gene_9652', 'gene_16133', 'gene_18906', 'gene_11713', 'gene_6836', 'gene_16342', 'gene_2318', 'gene_16130', 'gene_1545', 'gene_3695', 'gene_2910', 'gene_11354', 'gene_15795', 'gene_1331', 'gene_4737', 'gene_15896', 'gene_3737', 'gene_17738', 'gene_19313', 'gene_13507', 'gene_4224', 'gene_5729', 'gene_15894', 'gene_4618', 'gene_19377', 'gene_18091', 'gene_10128', 'gene_2844', 'gene_12847', 'gene_5453', 'gene_951', 'gene_15591', 'gene_18086', 'gene_9626', 'gene_88', 'gene_16372', 'gene_5578', 'gene_5944']



	list_KIRC = ["gene_14114", "gene_13818"] # retrieve specific genes of KIRC
	list_LUAD = ["gene_11903", "gene_15895"] # retrieve specific genes of LUAD

	# Plot a variable factor map for the first two dimensions.
	for i in range(0, pca.components_.shape[1]):
	
		# Display only four genes in the circle
		if df.columns[i] in important_genes:
			plt.arrow(0,
				0,  # Start the arrow at the origin
				pca.components_[0, i],  #0 for PC1
				pca.components_[1, i],  #1 for PC2
				head_width=0.03,
				head_length=0.03,
				color = "black")
				
			# Write the name of the gene in the plot	
			if df.columns[i] == 'gene_14114':
				print("\nCoordonnées (x,y) du gène 14114", pca.components_[0, i], pca.components_[1, i])
				plt.text(pca.components_[0, i] + 0.05, pca.components_[1, i] - 0.01, df.columns.values[i])
			elif df.columns[i] == 'gene_13818':
				print("\nCoordonnées (x,y) du gène 13818", pca.components_[0, i], pca.components_[1, i])
				plt.text(pca.components_[0, i] + 0.05, pca.components_[1, i] - 0.11, df.columns.values[i])
			elif df.columns[i] == 'gene_11903':
				print("\nCoordonnées (x,y) du gène 11903", pca.components_[0, i], pca.components_[1, i])
				plt.text(pca.components_[0, i] - 0.05, pca.components_[1, i] + 0.04, df.columns.values[i])
			elif df.columns[i] == 'gene_15895':
				print("\nCoordonnées (x,y) du gène 15895", pca.components_[0, i], pca.components_[1, i])
				plt.text(pca.components_[0, i] - 0.05, pca.components_[1, i] + 0.15, df.columns.values[i])

		# Print gene names with the same coordinates than gene_14114 and gene_13818
		#gène 14114 (0.03861437063443854 -0.008128849574310372)
		#gène 13818 (0.039505844701583234 -0.00905130786769864)
		if (0.038 < pca.components_[0, i] < 0.041) and (-0.009 < pca.components_[1, i] < -0.008) and (df.columns[i] not in ["gene_14114", "gene_13818"]):
			list_KIRC.append(df.columns[i])
			
		# Print gene names with the same coordinates than gene_11903 and gene_15895
		#Coordonnées (x,y) du gène 11903 -0.00329614908432472 0.019161190855232126
		# Coordonnées (x,y) du gène 15895 -0.0006446772013914009 0.02138124458959511
		if (-0.006 < pca.components_[0, i] < -0.003) and (0.019 < pca.components_[1, i] < 0.02) and (df.columns[i] not in ["gene_11903", "gene_15895"]):
			list_LUAD.append(df.columns[i])

		
	print("\nTumeur KIRC - gènes ressemblant à 14114 et 13818:", list_KIRC)
	print("\nTumeur LUAD - Gène ressemblant à 11903 et 15895:", list_LUAD)
			
	an = np.linspace(0, 2 * np.pi, 100)
	plt.plot(np.cos(an), np.sin(an))  # Add a unit circle for scale
	plt.axis('equal')
	plt.xlabel('PC1 (15,84%)')
	plt.ylabel('PC2 (10,50%)')
	#ax.set_title('Cercle de corrélation')
	plt.savefig('../output/ACP/variableFactorMap_{}components.png'.format(n_components), dpi=199)
	plt.show()
	
	return list_KIRC, list_LUAD
		
