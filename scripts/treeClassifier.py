import numpy as np
import matplotlib.pyplot as plt


from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import ExtraTreesClassifier




def test_parameter(X, Y):
	'''
	@param X: genetic expression of all genes
	@param Y: labels, type of cancer
	'''

	# n-estimators parameters to test
	list_n_estimators = [5, 10, 20, 30, 40, 50, 70, 80, 100, 120, 130, 150, 170, 185, 200, 250, 300, 350, 400, 450, 500]

	# Lists to save accuracy values
	list_accuracy_training = []	
	list_accuracy_testing = []	
	
	for n in list_n_estimators: 
	
		# Split data into train and test datas
		X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, stratify = Y, random_state=42)
		
		# Create model
		model = ExtraTreesClassifier(n_estimators=n, random_state=0)

		# Fit the model
		model.fit(X_train, y_train)

		# Test model
		Z = model.predict(X_test)
			
		# Calculate training accuracy
		training_accuracy = accuracy_score(y_train, model.predict(X_train))
		list_accuracy_training.append(training_accuracy)
		
		# Calculate testing accuracy
		testing_accuracy = accuracy_score(y_test, Z)
		list_accuracy_testing.append(testing_accuracy)
				
	# Plot accuracies according to n_estimators
	plt.style.use('ggplot')
	plt.plot(list_n_estimators, list_accuracy_training, color='red', label = 'Entraînement')
	plt.plot(list_n_estimators, list_accuracy_testing, color='blue', label = 'Test')
	plt.legend()
	plt.xlabel("Nombre d'arbres dans la forêt")
	plt.ylabel("Justesse")
	plt.savefig('../output/ExtraTreesClassifier/accuracyVSn-estimators.png')
	plt.show()	
