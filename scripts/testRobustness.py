import random
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np # for calculate means

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from sklearn.ensemble import ExtraTreesClassifier




def testRobustness(X, X_lessGenes, Y, nb_best_genes):
	'''
		Test robustness of the ExtraTreesClassifier method, removing genes
		@param X: all genes
		@param X_lessGenes: genes without the better
		@param nb_best_genes: the number of genes have been removed (for the legend in the plot)
	'''

	# Structures for save accuracy values
	training_accuracies = {}
	testing_accuracies = {}
	training_accuracies_lessGenes = {}
	testing_accuracies_lessGenes = {}
	
	list_training_accuracies = []
	list_testing_accuracies = []
	list_training_accuracies_lessGenes = []
	list_testing_accuracies_lessGenes = []
	
	# Proportions of genes to conserve
	proportions = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
	
	# Number of simulations to perform
	simulations = range(10)


	for i in proportions:
	
		for s in simulations:
		
			# Choice random columns betx
			random_columns = random.sample(range(0, len(X.columns)), int(len(X.columns)*(1-i))) 
			
			# Choice random columns for dataset without the important genes
			random_columns_lessGenes = random.sample(range(0, len(X_lessGenes.columns)), int(len(X_lessGenes.columns)*(1-i))) 
			
			# Delete corresponding columns
			X2 = X.drop(X.columns[random_columns], axis='columns')

			X_lessGenes2 = X_lessGenes.drop(X_lessGenes.columns[random_columns_lessGenes], axis='columns')

		    
		    	# Split data into train and test datas
		    	# For all genes
			X_train, X_test, y_train, y_test = train_test_split(X2, Y, test_size=0.33, stratify = Y, random_state=42)
			
			# For genes without better genes
			X_train_lessGenes, X_test_lessGenes, y_train_lessGenes, y_test_lessGenes = train_test_split(X_lessGenes2, Y, test_size=0.33, stratify = Y, random_state=42)
			
			# Create model
			model = ExtraTreesClassifier(n_estimators=100, random_state=0)
			model_lessGenes = ExtraTreesClassifier(n_estimators=100, random_state=0)
			
			# Fit the model
			model.fit(X_train, y_train)
			model_lessGenes.fit(X_train_lessGenes, y_train_lessGenes)
			
			# Test model
			Z = model.predict(X_test)
			Z_lessGenes = model_lessGenes.predict(X_test_lessGenes)
			
			# Calculate training accuracy
			training_accuracy = accuracy_score(y_train, model.predict(X_train))
			training_accuracy_lessGenes = accuracy_score(y_train_lessGenes, model_lessGenes.predict(X_train_lessGenes))
			# Calculate testing accuracy
			testing_accuracy = accuracy_score(y_test, Z)
			testing_accuracy_lessGenes = accuracy_score(y_test_lessGenes, Z_lessGenes)
			
			# All genes
			if i not in training_accuracies.keys():
				training_accuracies[i] = [training_accuracy]
			else:
				training_accuracies[i].append(training_accuracy)

			if i not in testing_accuracies.keys():
				testing_accuracies[i] = [testing_accuracy]
			else:
				testing_accuracies[i].append(testing_accuracy)
				
			# Genes without the important genes
			if i not in training_accuracies_lessGenes.keys():
				training_accuracies_lessGenes[i] = [training_accuracy_lessGenes]
			else:
				training_accuracies_lessGenes[i].append(training_accuracy_lessGenes)

			if i not in testing_accuracies_lessGenes.keys():
				testing_accuracies_lessGenes[i] = [testing_accuracy_lessGenes]
			else:
				testing_accuracies_lessGenes[i].append(testing_accuracy_lessGenes)


	# Calculate means
	for p in proportions:
		list_training_accuracies.append(np.mean(training_accuracies[p]))
		list_testing_accuracies.append(np.mean(testing_accuracies[p]))
		list_training_accuracies_lessGenes.append(np.mean(training_accuracies_lessGenes[p]))
		list_testing_accuracies_lessGenes.append(np.mean(testing_accuracies_lessGenes[p]))
		
		
	# Change proportions into %
	proportions2 = []
	for n in proportions:
		#proportions2.append("{} ({}%)".format(str(int(len(X.columns))*n), int(n*100)))
		proportions2.append(int(n*100))


	# Plot accuracies according to the number of genes
	plt.plot(proportions2, list_training_accuracies, color='red', label = '1. Entraînement')
	plt.plot(proportions2, list_testing_accuracies, color='red', linestyle='dashed', label = '1. Test')
	plt.plot(proportions2, list_training_accuracies_lessGenes, color='blue',  label = '2. Entraînement sans les {} meilleurs gènes'.format(nb_best_genes))
	plt.plot(proportions2, list_testing_accuracies_lessGenes, color='blue',  linestyle='dashed', label = '2. Test sans les {} meilleurs gènes'.format(nb_best_genes))
	plt.xticks(proportions2, rotation = 80, ha="right")
	plt.legend()
	plt.xlabel("Pourcentage de gènes conservés")
	plt.ylabel("Justesse")
	plt.style.use('ggplot')
	#plt.savefig('../output/robustness/testRobustness_{}genes.png'.format(nb_best_genes), dpi=300)
	plt.show()
