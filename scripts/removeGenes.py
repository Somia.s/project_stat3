import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.ensemble import ExtraTreesClassifier




def removeGenes(X, Y, list_genes, tumor_name):
	"""
		@param X: gene expression of all genes
		@param Y: type of tumors of each individuals
		@param list_genes: genes to remove
		@param tumor_name: tumor of the analysis
		Remove on gene and determine the precision of prediction for each type of cancer
	"""
	
	# Delete the gene
	X_dropped = X
	for name_gene in list_genes:
		X_dropped = X_dropped.drop([name_gene], axis=1)

	# Number of simulations to perform
	n_estimators = [50, 100, 150]

	for n in n_estimators:
		   
		#------------------------#
		# 	With all genes     #
		#------------------------#
		
		# Split data into train and test datas
		X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, stratify = Y, random_state=42)

		# Create model
		model = ExtraTreesClassifier(n_estimators=n, random_state=0)
			
		# Fit the model
		model.fit(X_train, y_train)
			
		# Test model
		Z = model.predict(X_test)
		
		# Evaluate model
		print("\n Evaluation du modèle avec tous les gènes: number of trees in the forest = {}.".format(n))
		table = pd.crosstab(y_test, Z)
		print(table.loc[[tumor_name]])
		
		#---------------------------------#
		# 	Withour the list of genes   #
		#---------------------------------#
	
		# Split data into train and test datas
		X_train, X_test, y_train, y_test = train_test_split(X_dropped, Y, test_size=0.33, stratify = Y, random_state=42)

		# Create model
		model = ExtraTreesClassifier(n_estimators=100, random_state=0)
			
		# Fit the model
		model.fit(X_train, y_train)
			
		# Test model
		Z = model.predict(X_test)
		
		# Evaluate model
		print("\n Evaluation du modèle sans les gènes {}: number of trees in the forest = {}.".format(list_genes, n))
		table = pd.crosstab(y_test, Z)
		print(table.loc[[tumor_name]])
		
	print("\n\n")
	
	
	
