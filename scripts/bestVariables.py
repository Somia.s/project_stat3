import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Tree Machine Learning
from sklearn.model_selection import train_test_split
from sklearn.ensemble import ExtraTreesClassifier





def importantFeatures(X, Y, nb_best_genes):
	'''
	Retrieve the importance of all features and select the better.
	@param X: gene expression of all genes
	@param Y: labels of types of cancer
	@param nb_best_genes: the number of best genes to select
	'''

	# Save genes ans their precisions
	best_genes_and_precisions = []
	
	dict_precision_means = {}
		
	# Split data into train and test datas
	X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, stratify = Y, random_state=42)
			
	# Create model
	model = ExtraTreesClassifier(n_estimators=100, random_state=0)
				
	# Train the model
	model.fit(X_train,y_train)

	# Evaluate the model
	Z = model.predict(X_test)
	#print(pd.crosstab(y_test,Z))
		
	# Take important features
	feat_importances = pd.DataFrame(model.feature_importances_, index = X.columns) # Use inbuilt class feature_importances of tree based classifiers
		
	# Select genes with the better precisions
	feat_importances = feat_importances.nlargest(nb_best_genes, columns = 0)

	# Plot graph of best features
	#feat_importances.plot(kind='barh')
	len_feat_importances = range(len(feat_importances))
	plt.bar(len_feat_importances, feat_importances[0])
	plt.xticks(len_feat_importances, feat_importances.index)
	plt.ylabel("Nom du gène")
	plt.xlabel("Importance d'un gène basée sur l'impureté des noeuds des arbres")
	plt.xticks(rotation = 15, ha="right")
	plt.style.use('ggplot')
	plt.savefig("../output/importantGenes/boxplotBestFeatures_{}genes.png".format(nb_best_genes), dpi=300)
	plt.show()

	# Return name of important genes
	return list(feat_importances.index)
	
	
	
	
	
def boxplotExpressionsGenes(gene, df):
	'''
	Perform Boxplot of gene expression according to the type of tumor
	@param gene: name of the gene
	@param df: genetic expression of genes for all individuals
	'''

	df_COAD = df.loc[df["Class"] == "COAD"]
	df_PRDA = df.loc[df["Class"] == "PRAD"]
	df_LUAD = df.loc[df["Class"] == "LUAD"]
	df_KIRC = df.loc[df["Class"] == "KIRC"]
	df_BRCA = df.loc[df["Class"] == "BRCA"]

	plt.boxplot([df_COAD[gene], df_PRDA[gene], df_LUAD[gene], df_KIRC[gene], df_BRCA[gene]])
	plt.xticks([1, 2, 3, 4, 5], ['COAD', 'PRAD', 'LUAD', 'KIRC', 'BRCA'])
	plt.style.use('ggplot')
	plt.ylabel("Expression génique")
	plt.xlabel("Type de cancer")
	plt.title("Gène {}".format(gene.replace("gene_", "")))
	plt.savefig("../output/importantGenes/{}.png".format(gene))
	plt.show()
