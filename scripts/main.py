import pandas as pd

from treeClassifier import *
from testRobustness import *
from bestVariables import *
from ACP import *
from removeGenes import *



def main():

	#---------------------------------#
	# %% Load data
	#---------------------------------#

	# Values of gene expressions
	df = pd.read_csv('../data/data.csv')
	# Labels: type of cancer
	df_labels = pd.read_csv('../data/labels.csv')
	

	
	
	#---------------------------------#
	# %% Data preparation
	#---------------------------------#
	
	# Remove columns with name samples
	X = df.iloc[:,1:]
	Y = df_labels["Class"]

	# Merge the labels and the values in the same table for the ACP perform
	total_df = pd.concat([X, Y], axis=1, join="inner")
	
	
	

	#---------------------------------#
	# %% Evaluate parameter n_estimator of the Extra Tree Classifier method
	#---------------------------------#
	
	test_parameter(X, Y)
	



	#---------------------------------#
	# %% Evaluate ACP according to number of ACP components chosen
	#---------------------------------#

	accuracyVSnb_components(X, Y)

	# Explain variances according to the principal composant
	screePlot(X)




	#---------------------------------#
	# %% Determine important features/genes
	#---------------------------------#
	
	
	for nb_best_genes in [10, 50, 100]:
		importantGenes = importantFeatures(X, Y, nb_best_genes)
		
		print("\nLes {} premiers gènes les plus importants d'après la méthoode d'arbres de décision sont: {}".format(nb_best_genes, importantGenes))
	
		#---------------------------------#
		# %% Then test robustness without those best genes
		#---------------------------------#	
		df_not_importantGenes = df.drop(importantGenes, axis=1)
		X_lessGenes = df_not_importantGenes.iloc[:,1:]
		testRobustness(X, X_lessGenes, Y, nb_best_genes)
	
	
	
	
	
	#---------------------------------#
	# %% Do boxplot of genetic expression inside a gene, for each type of cancer
	# for the 10 better genes only
	#---------------------------------#
	
	importantGenes = ['gene_14114', 'gene_19296', 'gene_7792', 'gene_2774', 'gene_2639', 'gene_13818', 'gene_15895', 'gene_15633', 'gene_9184', 'gene_11903'] # according to importantFeatures function
	
	for gene in importantGenes:
		boxplotExpressionsGenes(gene, total_df)
	



	#---------------------------------#
	# %% Perform ACP with 2 principal components
	#---------------------------------#
	
	
	
	list_KIRC, list_LUAD = ACP(total_df, 2)


	
	#---------------------------------#
	# %% Remove an important genes and evaluate the classifier
	#---------------------------------#
	
	# KIRC tumor
	removeGenes(X, Y, list_KIRC, "KIRC")
	
	# LUAD tumor
	removeGenes(X, Y, list_LUAD, "LUAD")
	

main()
