# Prediction of a type of cancer by machine learning

## Prerequises

```
pip3 install pandas
pip3 install numpy
pip3 install matplotlib
pip3 install sk-learn
```

## Architecture

```
predictCancer
├── data
├── scripts
├── output
│   └── ACP
│   └── ExtraTreesClassifier
│   └── ImportantGenes
│   └── robustness
├── doc
```

## Running

```
pyton3 main.py
```

Input: two CSV file, the data are from PANCA data set.
- "data.csv": genetic expression of 20531 genes of 800 patients.
- "labels.csv": the type of cancer of 800 patients.

## Analysis

### Evaluate a paramater of the extra tree classifier method from Sklearn

- Script: "treeClassifier.py"
- According to different value of a parameter from the extra tree classifier method, show the training and testing accuracy.
- Parameter value called "n_estimators" is the number of trees in the forest.
- Output: A plot; the accuracy according to this parameter value.

### Retrieve the better features according to the impurity in the trees

- Script: "bestVariables.py"
- Determine the better gene variables in the extra tree classifier method, with the better impurity i.e. as little heterogeneity as possible after splitting a group of samples into two groups at each node of the tree.
- Outputs:
	- A barplot: Importance of the gene feature according to the gene name
	- Boxplots of the genetic expressions according to the cancer type

### Test robustness of the method, according to the size of the data

- Script: "testRobustness.py"
- Outputs: The training and testing accuracy, in terms of the number of genes retained; for the 10, 50 and 100 better genes.

### Perform an ACP

- Script: "ACP.py"
- Perform an ACP into 2 principal components.
- Ouputs:
	- Plot the accuracies according to the number of ACP components.
	- Plot the explained variance by a principal component, for different ACP where the number of principal components chosen is different.
	- The points in terms of the first two PCs.
	- The variable factor map for the first two dimensions.

### Remove an important genes and evaluate the classifier

- Script: "removeGenes.py"
- Remove a list of genes, and return the comparison between the prediction with the all genes and the prediction without those genes (perform for 3 differents values of the number of trees composing the forest).
